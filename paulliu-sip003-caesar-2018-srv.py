#!/usr/bin/env python3

import os
import logging
import socket
import threading
import select
import queue

"""
凱薩編碼法, 加密函式

@param data 資料, bytes 陣列
@param key 密鑰
@return 加密資料
"""
def caesarEnc(data, key=3):
    ret = []
    for c in data:
        ret += bytes([ (int(c)+key)%256 ])
    return bytes(ret)

"""
凱薩編碼法, 解密函式

@param data 資料, bytes 陣列
@param key 密鑰
@return 解密資料
"""
def caesarDec(data, key=3):
    ret = []
    for c in data:
        ret += bytes([ ((int(c) + 256) - key)%256 ])
    return bytes(ret)

"""
資料傳輸函式

remoteSocket 是 server accept 出來的 socket. 對應一個遠端連上來的 client.
我們必須建立一個連往 local shadowsocks 開出的 port.
如果 remoteSocket 有收到資料, 就解密往 local shadowsocks 傳
如果 local shadowsocks 有資料, 就加密往 remoteSocket 傳

@param remoteSocket 對應遠端連上的 client
@param threadID thread 的 ID
@param threadQueue 用來通知主 thread 我們結束了的 queue
"""
def dataTransfer(remoteSocket, threadID, threadQueue):
    # 連往 local shadowsocks
    localSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    localSocket.connect((os.environ["SS_LOCAL_HOST"], int(os.environ["SS_LOCAL_PORT"])))

    try:
        while True:
            readable, _, except1 = select.select([localSocket, remoteSocket], [], [localSocket, remoteSocket])
            # 如果有 exception 就跳出
            if ((localSocket in except1) or (remoteSocket in except1)):
                break
            # 如果 remoteSocket 有資料
            if (remoteSocket in readable):
                data = remoteSocket.recv(612)
                if (len(data) == 0):
                    break
                localSocket.send(caesarDec(data))
            # 如果 localSocket (shadowsocks 端) 有資料
            if (localSocket in readable):
                data = localSocket.recv(517)
                if (len(data) == 0):
                    break
                remoteSocket.send(caesarEnc(data))
    except:
        pass
    threadQueue.put(threadID)

def main():
    logging.basicConfig(level=logging.DEBUG)

    # 列印環境變數
    if ("SS_LOCAL_HOST" in os.environ):
        logging.info("SS_LOCAL_HOST: %s"%(os.environ["SS_LOCAL_HOST"]))
    if ("SS_LOCAL_PORT" in os.environ):
        logging.info("SS_LOCAL_PORT: %s"%(os.environ["SS_LOCAL_PORT"]))
    if ("SS_REMOTE_HOST" in os.environ):
        logging.info("SS_REMOTE_HOST: %s"%(os.environ["SS_REMOTE_HOST"]))
    if ("SS_REMOTE_PORT" in os.environ):
        logging.info("SS_REMOTE_PORT: %s"%(os.environ["SS_REMOTE_PORT"]))

    # 建立 server socket.
    remoteServerSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    remoteServerSocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    remoteServerSocket.bind((os.environ["SS_REMOTE_HOST"], int(os.environ["SS_REMOTE_PORT"])))
    remoteServerSocket.listen(5)

    threadID = 0
    threads = {}
    threadQueue = queue.Queue()

    # 無限迴圈等待 client 連上
    while True:
        # 接受一個 remote client.
        remoteSocket, _ = remoteServerSocket.accept()
        logging.info("Accepting remote connection")
        # 建立一個 thread, 用來處理 remote client.
        threadID = threadID + 1
        t = threading.Thread(target = dataTransfer, args=[remoteSocket, threadID, threadQueue])
        threads[threadID] = t
        logging.info("Thread %d start, total %d threads"%(threadID, len(threads)))
        t.start()
        # join 一下之前已經結束的 thread.
        while (not threadQueue.empty()):
            tEnd = threadQueue.get()
            logging.info("Thread %d joined"%(tEnd))
            threads[tEnd].join()
            threads.pop(tEnd)
            logging.info("%d threads lefting", len(threads))

if __name__ == '__main__':
    main()
