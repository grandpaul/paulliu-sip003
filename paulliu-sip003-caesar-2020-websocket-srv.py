#!/usr/bin/env python3

import os
import logging
import asyncio
import websockets

"""
凱薩編碼法, 加密函式

@param data 資料, bytes 陣列
@param key 密鑰
@return 加密資料
"""
def caesarEnc(data, key=3):
    ret = []
    for c in data:
        ret += bytes([ (int(c)+key)%256 ])
    return bytes(ret)

"""
凱薩編碼法, 解密函式

@param data 資料, bytes 陣列
@param key 密鑰
@return 解密資料
"""
def caesarDec(data, key=3):
    ret = []
    for c in data:
        ret += bytes([ ((int(c) + 256) - key)%256 ])
    return bytes(ret)

async def move_data_from_local_to_remote(reader, websocket):
    while True:
        try:
            data = await reader.read(100)
            if (len(data)==0):
                break
            await websocket.send(caesarEnc(data))
        except:
            break

async def move_data_from_remote_to_local(writer, websocket):
    while True:
        try:
            data = await websocket.recv()
            if (len(data)==0):
                break
            writer.write(caesarDec(data))
            await writer.drain()
        except:
            break

async def handle_connect(websocket, path):
    loop = asyncio.get_event_loop()
    reader, writer = await asyncio.open_connection(os.environ["SS_LOCAL_HOST"], int(os.environ["SS_LOCAL_PORT"]), loop=loop)
    await asyncio.gather(move_data_from_local_to_remote(reader, websocket),\
 move_data_from_remote_to_local(writer, websocket))

def main():
    logging.basicConfig(level=logging.DEBUG)

    # 列印環境變數
    if ("SS_LOCAL_HOST" in os.environ):
        logging.info("SS_LOCAL_HOST: %s"%(os.environ["SS_LOCAL_HOST"]))
    if ("SS_LOCAL_PORT" in os.environ):
        logging.info("SS_LOCAL_PORT: %s"%(os.environ["SS_LOCAL_PORT"]))
    if ("SS_REMOTE_HOST" in os.environ):
        logging.info("SS_REMOTE_HOST: %s"%(os.environ["SS_REMOTE_HOST"]))
    if ("SS_REMOTE_PORT" in os.environ):
        logging.info("SS_REMOTE_PORT: %s"%(os.environ["SS_REMOTE_PORT"]))

    start_server = websockets.serve(handle_connect, os.environ["SS_REMOTE_HOST"], int(os.environ["SS_REMOTE_PORT"]))

    asyncio.get_event_loop().run_until_complete(start_server)
    asyncio.get_event_loop().run_forever()

if __name__ == '__main__':
    main()
