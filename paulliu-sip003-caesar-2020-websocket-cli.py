#!/usr/bin/env python3

import os
import logging
import asyncio
import websockets

"""
凱薩編碼法, 加密函式

@param data 資料, bytes 陣列
@param key 密鑰
@return 加密資料
"""
def caesarEnc(data, key=3):
    ret = []
    for c in data:
        ret += bytes([ (int(c)+key)%256 ])
    return bytes(ret)

"""
凱薩編碼法, 解密函式

@param data 資料, bytes 陣列
@param key 密鑰
@return 解密資料
"""
def caesarDec(data, key=3):
    ret = []
    for c in data:
        ret += bytes([ ((int(c) + 256) - key)%256 ])
    return bytes(ret)

async def move_data_from_local_to_remote(reader, websocket):
    while True:
        try:
            data = await reader.read(100)
            if (len(data)==0):
                break
            await websocket.send(caesarEnc(data))
        except:
            break

async def move_data_from_remote_to_local(writer, websocket):
    while True:
        try:
            data = await websocket.recv()
            if (len(data)==0):
                break
            writer.write(caesarDec(data))
            await writer.drain()
        except:
            break
        
async def handle_connect(reader, writer):

    uri = "ws://%s:%d/paulliu/2020/caesar"%(os.environ["SS_REMOTE_HOST"], int(os.environ["SS_REMOTE_PORT"]))
    async with websockets.connect(uri) as websocket:
        await asyncio.gather(move_data_from_local_to_remote(reader, websocket), move_data_from_remote_to_local(writer, websocket))
    print("Close the client socket")
    writer.close()
    
def main():
    logging.basicConfig(level=logging.DEBUG)

    # 列印環境變數
    if ("SS_LOCAL_HOST" in os.environ):
        logging.info("SS_LOCAL_HOST: %s"%(os.environ["SS_LOCAL_HOST"]))
    if ("SS_LOCAL_PORT" in os.environ):
        logging.info("SS_LOCAL_PORT: %s"%(os.environ["SS_LOCAL_PORT"]))
    if ("SS_REMOTE_HOST" in os.environ):
        logging.info("SS_REMOTE_HOST: %s"%(os.environ["SS_REMOTE_HOST"]))
    if ("SS_REMOTE_PORT" in os.environ):
        logging.info("SS_REMOTE_PORT: %s"%(os.environ["SS_REMOTE_PORT"]))

    loop = asyncio.get_event_loop()
    coro = asyncio.start_server(handle_connect, os.environ["SS_LOCAL_HOST"], int(os.environ["SS_LOCAL_PORT"]), loop=loop)
    server = loop.run_until_complete(coro)

    # Serve requests until Ctrl+C is pressed
    logging.info('Serving on {}'.format(server.sockets[0].getsockname()))
    try:
        loop.run_forever()
    except KeyboardInterrupt:
        pass

    # Close the server
    server.close()
    loop.run_until_complete(server.wait_closed())
    loop.close()


if __name__ == '__main__':
    main()
